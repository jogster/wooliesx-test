﻿using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Shouldly;
using System.Net.Http;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Wrappers;
using WooliesX.BusinessLogic.Services;

namespace WooliesX.BusinessLogic.Tests.Services
{
    public class TrolleyTotalServiceTests
    {
        private TrolleyTotalService _sut;
        private Mock<ILogger<TrolleyTotalService>> logger;
        private Mock<IHttpClientWrapper> httpClient;
        private Mock<IConfigurationService> configService;

        [SetUp]
        public void Setup()
        {
            logger = new Mock<ILogger<TrolleyTotalService>>();
            httpClient = new Mock<IHttpClientWrapper>();
            configService = new Mock<IConfigurationService>();
            _sut = new TrolleyTotalService(logger.Object, httpClient.Object, configService.Object);
        }

        [Test]
        public async Task GivenValidJsonRequest_ComputeTrolleyTotalWithCalculatorApi_CorrectProcessFollowed()
        {
            var testJson = "{\"Products\":[{\"Name\":\"1\",\"Price\":2.0},{\"Name\":\"2\",\"Price\":5.0}],\"Specials\":[{\"Quantities\":[{\"Name\":\"1\",\"Quantity\":3},{\"Name\":\"2\",\"Quantity\":0}],\"Total\":5.0},{\"Quantities\":[{\"Name\":\"1\",\"Quantity\":1},{\"Name\":\"2\",\"Quantity\":2}],\"Total\":10.0}],\"Quantities\":[{\"Name\":\"1\",\"Quantity\":3},{\"Name\":\"2\",\"Quantity\":2}]}";

            var responseMessage = new HttpResponseMessage()
            {
                Content = new StringContent("333"),
                StatusCode = System.Net.HttpStatusCode.OK,
            };

            this.configService.Setup(x => x.GetConfigurationValue("uniqueId")).ReturnsAsync("123");
            this.configService.Setup(x => x.GetConfigurationValue("trolleyTotalResourceBaseUrl")).ReturnsAsync("http://wooliesx.test.azure.com/trolleyCalculator?token=");
            this.httpClient.Setup(x => x.PostAsync("http://wooliesx.test.azure.com/trolleyCalculator?token=123", It.IsAny<StringContent>())).ReturnsAsync(responseMessage);

            var result = await _sut.ComputeTrolleyTotalWithCalculatorApi(testJson);

            result.ShouldNotBeNull();
            result.ShouldBe("333");
            configService.Verify(x => x.GetConfigurationValue("uniqueId"), Times.Once);
            configService.Verify(x => x.GetConfigurationValue("trolleyTotalResourceBaseUrl"), Times.Once);
            httpClient.Verify(x => x.PostAsync("http://wooliesx.test.azure.com/trolleyCalculator?token=123", It.IsAny<StringContent>()), Times.Once);
        }
    }
}
