﻿using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Interfaces.Exceptions;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Sorting;
using WooliesX.BusinessLogic.Interfaces.Wrappers;
using WooliesX.BusinessLogic.Services;

namespace WooliesX.BusinessLogic.Tests.Services
{
    public class ProductSortServiceTests
    {
        private ProductSortService _sut;
        private Mock<ILogger<ProductSortService>> logger;
        private Mock<IHttpClientWrapper> httpClient;
        private Mock<ISortingFactory> sortingFactory;
        private Mock<IConfigurationService> configService;
        private Mock<ISortingStrategy> sorter;

        [SetUp]
        public void Setup()
        {
            logger = new Mock<ILogger<ProductSortService>>();
            httpClient = new Mock<IHttpClientWrapper>();
            sortingFactory = new Mock<ISortingFactory>();
            configService = new Mock<IConfigurationService>();
            sorter = new Mock<ISortingStrategy>();
            _sut = new ProductSortService(logger.Object, httpClient.Object, sortingFactory.Object, configService.Object);
        }

        [Test]
        public async Task SortByPriceLowtoHighRequested_SortProducts_CorrectSortingApplied()
        {
            var testProductData = CreateTestData();

            var testProductDataJson = JsonConvert.SerializeObject(testProductData);

            var responseMessage = new HttpResponseMessage()
            {
                Content = new StringContent(testProductDataJson),
                StatusCode = System.Net.HttpStatusCode.OK,
            };

            var dummyResult = testProductData.OrderBy(x => x.Price);

            this.configService.Setup(x => x.GetConfigurationValue("uniqueId")).ReturnsAsync("123");
            this.configService.Setup(x => x.GetConfigurationValue("productsResourceBaseUrl")).ReturnsAsync("http://wooliesx.test.azure.com/products?token=");
            this.httpClient.Setup(x => x.GetAsync("http://wooliesx.test.azure.com/products?token=123")).ReturnsAsync(responseMessage);

            this.sortingFactory.Setup(x => x.CreateSorter(Interfaces.Enums.SortingType.Low)).Returns(sorter.Object);
            this.sorter.Setup(x => x.SortCustomerProducts(It.IsAny<IList<Product>>())).ReturnsAsync(dummyResult);

            var result = await _sut.SortProducts("low");

            result.ShouldBe(dummyResult);
            this.configService.Verify(x => x.GetConfigurationValue("uniqueId"), Times.Once);
            this.configService.Verify(x => x.GetConfigurationValue("productsResourceBaseUrl"), Times.Once);
            this.httpClient.Verify(x => x.GetAsync("http://wooliesx.test.azure.com/products?token=123"), Times.Once);
            this.sortingFactory.Verify(x => x.CreateSorter(Interfaces.Enums.SortingType.Low), Times.Once);
            this.sorter.Verify(x => x.SortCustomerProducts(It.IsAny<IList<Product>>()), Times.Once);
        }

        [Test]
        public async Task UnknownSortingStringProvided_SortProducts_WooliesExceptionWithCorrectReasonThrown()
        {
            var exception = await Should.ThrowAsync<WooliesException>(() => _sut.SortProducts("abcdefg"));

            exception.ShouldNotBeNull();
            exception.Reason.ShouldBe(Interfaces.Enums.FaultReason.UnknownSortingType);
        }

        private List<Product> CreateTestData()
        {
            return new List<Product>()
            {
                new Product()
                {
                    Name = "Test Product A",
                    Price = 99.99,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product B",
                    Price = 101.99,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product C",
                    Price = 10.99,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product D",
                    Price = 5,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product F",
                    Price = 999999999999,
                    Quantity = 0,
                },
            };
        }
    }
}
