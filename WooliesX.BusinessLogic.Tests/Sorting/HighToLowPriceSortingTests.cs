﻿using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Sorting;

namespace WooliesX.BusinessLogic.Tests.Sorting
{
    public class LowToHighPriceSortingTests
    {
        private LowToHighPriceSorting _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new LowToHighPriceSorting();
        }

        [Test]
        public async Task GivenValidListOfCustomerProducts_SortCustomerProducts_ProductsSortedLowToHighByPrice()
        {
            var testData = new List<Product>()
            {
                new Product()
                {
                    Name = "Product X",
                    Price = 101,
                    Quantity = 10,
                },
                new Product()
                {
                    Name = "Product A",
                    Price = 20,
                    Quantity = 20,
                },
                new Product()
                {
                    Name = "Product V",
                    Price = 31,
                    Quantity = 30,
                }
            };

            var result = (await _sut.SortCustomerProducts(testData)).ToList();

            result.ShouldNotBeNull();
            result[0].Name.ShouldBe("Product A");
            result[0].Price.ShouldBe(20);
            result[0].Quantity.ShouldBe(20);
            result[1].Name.ShouldBe("Product V");
            result[1].Price.ShouldBe(31);
            result[1].Quantity.ShouldBe(30);
            result[2].Name.ShouldBe("Product X");
            result[2].Price.ShouldBe(101);
            result[2].Quantity.ShouldBe(10);
        }

        [Test]
        public async Task GivenEmptyListOfCustomerProducts_SortCustomerProducts_ReturnsEmptyCollectionOfProducts()
        {
            var testData = new List<Product>(){};

            var result = await _sut.SortCustomerProducts(testData);
            var resultList = result.ToList();

            result.ShouldNotBeNull();
            result.Count().ShouldBe(0);
        }
    }
}