﻿using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Wrappers;
using WooliesX.BusinessLogic.Sorting;

namespace WooliesX.BusinessLogic.Tests.Sorting
{
    public class RecommendedProductSortingTests
    {
        private RecommendedProductsSorting _sut;

        private Mock<IHttpClientWrapper> httpClient;
        private Mock<IConfigurationService> configService;

        [SetUp]
        public void Setup()
        {
            httpClient = new Mock<IHttpClientWrapper>();
            configService = new Mock<IConfigurationService>();
            _sut = new RecommendedProductsSorting(httpClient.Object, configService.Object);
        }

        [Test]
        public async Task GivenSameShoppingHistoryAndProductsListDatasetFromSwagger_SortCustomerProducts_ProductsSortedHighToLowTotalProductQuantity()
        {
            var testCustomerPurchaseHistory = new List<CustomerProducts>()
            {
                new CustomerProducts()
                {
                    CustomerId = 123,
                    ShoppingProducts = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "Test Product A",
                            Price = 99.99,
                            Quantity = 3
                        },
                        new Product()
                        {
                            Name = "Test Product B",
                            Price = 101.99,
                            Quantity = 1
                        },
                        new Product()
                        {
                            Name = "Test Product F",
                            Price = 999999999999,
                            Quantity = 1
                        },
                    }
                },
                new CustomerProducts()
                {
                    CustomerId = 23,
                    ShoppingProducts = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "Test Product A",
                            Price = 99.99,
                            Quantity = 2
                        },
                        new Product()
                        {
                            Name = "Test Product B",
                            Price = 101.99,
                            Quantity = 3
                        },
                        new Product()
                        {
                            Name = "Test Product F",
                            Price = 999999999999,
                            Quantity = 1
                        },
                    }
                },
                new CustomerProducts()
                {
                    CustomerId = 23,
                    ShoppingProducts = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "Test Product C",
                            Price = 10.99,
                            Quantity = 2
                        },
                        new Product()
                        {
                            Name = "Test Product F",
                            Price = 999999999999,
                            Quantity = 2
                        },
                    }
                },
                new CustomerProducts()
                {
                    CustomerId = 23,
                    ShoppingProducts = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "Test Product A",
                            Price = 99.99,
                            Quantity = 1
                        },
                        new Product()
                        {
                            Name = "Test Product B",
                            Price = 101.99,
                            Quantity = 1
                        },
                        new Product()
                        {
                            Name = "Test Product C",
                            Price = 10.99,
                            Quantity = 1
                        },
                    }
                },
            };

            var testProductData = new List<Product>()
            {
                new Product()
                {
                    Name = "Test Product A",
                    Price = 99.99,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product B",
                    Price = 101.99,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product C",
                    Price = 10.99,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product D",
                    Price = 5,
                    Quantity = 0,
                },
                new Product()
                {
                    Name = "Test Product F",
                    Price = 999999999999,
                    Quantity = 0,
                },
            };

            var shopperHistoryJson = JsonConvert.SerializeObject(testCustomerPurchaseHistory);

            var responseMessage = new HttpResponseMessage()
            {
                Content = new StringContent(shopperHistoryJson),
                StatusCode = System.Net.HttpStatusCode.OK,
            };

            this.configService.Setup(x => x.GetConfigurationValue("uniqueId")).ReturnsAsync("123");
            this.configService.Setup(x => x.GetConfigurationValue("shopperHistoryResourceBaseUrl")).ReturnsAsync("http://wooliesx.test.azure.com/shopperHistory?token=");
            this.httpClient.Setup(x => x.GetAsync("http://wooliesx.test.azure.com/shopperHistory?token=123")).ReturnsAsync(responseMessage);

            var result = (await _sut.SortCustomerProducts(testProductData)).ToList();

            result.ShouldNotBeNull();

            result[0].Name.ShouldBe("Test Product A");
            result[0].Price.ShouldBe(99.99);
            result[0].Quantity.ShouldBe(6);
            result[1].Name.ShouldBe("Test Product B");
            result[1].Price.ShouldBe(101.99);
            result[1].Quantity.ShouldBe(5);
            result[2].Name.ShouldBe("Test Product F");
            result[2].Price.ShouldBe(999999999999);
            result[2].Quantity.ShouldBe(4);
            result[3].Name.ShouldBe("Test Product C");
            result[3].Price.ShouldBe(10.99);
            result[3].Quantity.ShouldBe(3);
            result[4].Name.ShouldBe("Test Product D");
            result[4].Price.ShouldBe(5);
            result[4].Quantity.ShouldBe(0);
            this.configService.Verify(x => x.GetConfigurationValue("uniqueId"), Times.Once);
            this.configService.Verify(x => x.GetConfigurationValue("shopperHistoryResourceBaseUrl"), Times.Once);
            this.httpClient.Verify(x => x.GetAsync("http://wooliesx.test.azure.com/shopperHistory?token=123"), Times.Once);
        }
    }
}