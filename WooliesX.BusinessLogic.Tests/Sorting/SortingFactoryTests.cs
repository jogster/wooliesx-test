﻿using Moq;
using NUnit.Framework;
using Shouldly;
using WooliesX.BusinessLogic.Interfaces.Exceptions;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Wrappers;
using WooliesX.BusinessLogic.Sorting;

namespace WooliesX.BusinessLogic.Tests.Sorting
{
    public class SortingFactoryTests
    {
        private SortingFactory _sut;
        private Mock<IHttpClientWrapper> httpClient;
        private Mock<IConfigurationService> configService;

        [SetUp]
        public void Setup()
        {
            httpClient = new Mock<IHttpClientWrapper>();
            configService = new Mock<IConfigurationService>();
            _sut = new SortingFactory(httpClient.Object, configService.Object);
        }

        [Test]
        public void GivenSortingTypeIsAscendingName_CreateSorter_AscendingNameSortingSorterShouldBeReturned()
        {
            var sorter = _sut.CreateSorter(Interfaces.Enums.SortingType.Ascending);

            sorter.GetType().ShouldBe(typeof(AscendingNameSorting));
        }

        [Test]
        public void GivenSortingTypeIsDescendingName_CreateSorter_DescendingNameSortingSorterShouldBeReturned()
        {
            var sorter = _sut.CreateSorter(Interfaces.Enums.SortingType.Descending);

            sorter.GetType().ShouldBe(typeof(DescendingNameSorting));
        }

        [Test]
        public void GivenSortingTypeIsHighToLowPrice_CreateSorter_LowToHighPriceSortingSorterShouldBeReturned()
        {
            var sorter = _sut.CreateSorter(Interfaces.Enums.SortingType.High);

            sorter.GetType().ShouldBe(typeof(HighToLowSorting));
        }

        [Test]
        public void GivenSortingTypeIsLowToHighPrice_CreateSorter_LowToHighPriceSortingSorterShouldBeReturned()
        {
            var sorter = _sut.CreateSorter(Interfaces.Enums.SortingType.Low);

            sorter.GetType().ShouldBe(typeof(LowToHighPriceSorting));
        }

        [Test]
        public void GivenSortingTypeIsRecommendedProduct_CreateSorter_RecommendedProductSortingSorterShouldBeReturned()
        {
            var sorter = _sut.CreateSorter(Interfaces.Enums.SortingType.Recommended);

            sorter.GetType().ShouldBe(typeof(RecommendedProductsSorting));
        }

        [Test]
        public void GivenSortingTypeIsUnknown_CreateSorter_WooliesExceptionWithCorrectReasonShouldBeReturned()
        {
            var exception = Should.Throw<WooliesException>(() => _sut.CreateSorter(Interfaces.Enums.SortingType.Unknown));

            exception.ShouldNotBeNull();
            exception.Reason.ShouldBe(Interfaces.Enums.FaultReason.UnknownSortingType);
        }

    }
}
