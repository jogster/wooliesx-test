﻿using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Sorting;

namespace WooliesX.BusinessLogic.Tests.Sorting
{
    public class DescendingNameSortingTests
    {
        private DescendingNameSorting _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new DescendingNameSorting();
        }

        [Test]
        public async Task GivenValidListOfCustomerProducts_SortCustomerProducts_ProductsSortedDescendingByName()
        {
            var testData = new List<Product>()
            {
                new Product()
                {
                    Name = "Product X",
                    Price = 1,
                    Quantity = 10,
                },
                new Product()
                {
                    Name = "Product A",
                    Price = 2,
                    Quantity = 20,
                },
                new Product()
                {
                    Name = "Product V",
                    Price = 3,
                    Quantity = 30,
                }
            };

            var result = (await _sut.SortCustomerProducts(testData)).ToList();

            result.ShouldNotBeNull();
            result[0].Name.ShouldBe("Product X");
            result[0].Price.ShouldBe(1);
            result[0].Quantity.ShouldBe(10);
            result[1].Name.ShouldBe("Product V");
            result[1].Price.ShouldBe(3);
            result[1].Quantity.ShouldBe(30);
            result[2].Name.ShouldBe("Product A");
            result[2].Price.ShouldBe(2);
            result[2].Quantity.ShouldBe(20);
        }

        [Test]
        public async Task GivenEmptyListOfCustomerProducts_SortCustomerProducts_ReturnsEmptyCollectionOfProducts()
        {
            var testData = new List<Product>(){};

            var result = await _sut.SortCustomerProducts(testData);
            var resultList = result.ToList();

            result.ShouldNotBeNull();
            result.Count().ShouldBe(0);
        }
    }
}
