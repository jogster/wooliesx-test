﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents a customer and products previously bought.
    /// </summary>
    public class CustomerProducts
    {
        /// <summary>
        /// The customer's unique ID.
        /// </summary>
        [JsonProperty("customerId")]
        public long CustomerId { get; set; }

        /// <summary>
        /// The list of products for the customer.
        /// </summary>
        [JsonProperty("products")]
        public IList<Product> ShoppingProducts { get; set; }
    }
}