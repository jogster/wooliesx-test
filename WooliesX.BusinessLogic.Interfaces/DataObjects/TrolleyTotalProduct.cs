﻿using Newtonsoft.Json;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents the Product object on TrolleyTotal request.
    /// </summary>
    public class TrolleyTotalProduct
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("Name")]
        public long Name { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        [JsonProperty("Price")]
        public long Price { get; set; }
    }
}