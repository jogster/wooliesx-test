﻿using Newtonsoft.Json;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents a Product DTO.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// The product's name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The product's price.
        /// </summary>
        [JsonProperty("price")]
        public double Price { get; set; }

        /// <summary>
        /// The product's quantity.
        /// </summary>
        [JsonProperty("quantity")]
        public long Quantity { get; set; }
    }
}