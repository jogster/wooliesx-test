﻿using Newtonsoft.Json;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents a SortProduct request POST payload.
    /// </summary>
    public class SortProductsRequest
    {
        /// <summary>
        /// Gets or sets the sort option.
        /// </summary>
        [JsonPropertyAttribute("sortOption")]
        public string SortOption { get; set; }
    }
}