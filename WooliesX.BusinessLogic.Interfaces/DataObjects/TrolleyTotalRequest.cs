﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents the TrolleyTotal request object.
    /// </summary>
    public class TrolleyTotalRequest
    {
        /// <summary>
        /// Products.
        /// </summary>
        [JsonProperty("Products")]
        public IList<TrolleyTotalProduct> Products { get; set; }

        /// <summary>
        /// Gets or sets the specials.
        /// </summary>
        [JsonProperty("Specials")]
        public IList<Special> Specials { get; set; }

        /// <summary>
        /// Gets or sets the quantities.
        /// </summary>
        [JsonProperty("Quantities")]
        public IList<Quantity> Quantities { get; set; }
    }
}
