﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents the Special object on TrolleyTotal request.
    /// </summary>
    public class Special
    {
        [JsonProperty("Quantities")]
        public IList<Quantity> Quantities { get; set; }

        [JsonProperty("Total")]
        public long Total { get; set; }
    }
}