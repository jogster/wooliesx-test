﻿using Newtonsoft.Json;

namespace WooliesX.BusinessLogic.Interfaces.DataObjects
{
    /// <summary>
    /// Represents the Quantity object on TrolleyTotal request.
    /// </summary>
    public class Quantity
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("Name")]
        public long Name { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        [JsonProperty("Quantity")]
        public long ProductQuantity { get; set; }
    }

}
