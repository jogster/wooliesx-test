﻿using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.ViewModels;

namespace WooliesX.BusinessLogic.Interfaces.Builders
{
    /// <summary>
    /// Provides interface definitions for /user API path.
    /// </summary>
    public interface IUserBuilder
    {
        /// <summary>
        /// Create a <see cref="UserDetails"/> object.
        /// </summary>
        /// <param name="name">The name field.</param>
        /// <param name="token">The token/unique id field.</param>
        /// <returns></returns>
        Task<UserDetails> CreateUserDetails(string name, string token);
    }
}
