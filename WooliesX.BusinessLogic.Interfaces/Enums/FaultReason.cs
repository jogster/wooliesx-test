﻿namespace WooliesX.BusinessLogic.Interfaces.Enums
{
    /// <summary>
    /// Enumeration of various fault reasons within WooliesX application.
    /// </summary>
    public enum FaultReason
    {
        UnknownSortingType,
    }
}
