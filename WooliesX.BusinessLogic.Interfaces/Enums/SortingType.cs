﻿namespace WooliesX.BusinessLogic.Interfaces.Enums
{
    /// <summary>
    /// Enumerations for various sorting types.
    /// </summary>
    public enum SortingType
    {
        Low,
        High,
        Ascending,
        Descending,
        Recommended,
        Unknown
    }
}