﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;

namespace WooliesX.BusinessLogic.Interfaces.Sorting
{
    /// <summary>
    /// Provides interface definition for sorting strategy implementation.
    /// </summary>
    public interface ISortingStrategy
    {
        /// <summary>
        /// Sorts the given collection of customer products.
        /// </summary>
        /// <param name="productsList">List of products to sort.</param>
        /// <returns>A collection of sorted products.</returns>
        Task<IOrderedEnumerable<Product>> SortCustomerProducts(IList<Product> productsList);
    }
}
