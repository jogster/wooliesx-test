﻿using WooliesX.BusinessLogic.Interfaces.Enums;

namespace WooliesX.BusinessLogic.Interfaces.Sorting
{
    /// <summary>
    /// Provides interface definition for providing sorting strategy implementation.
    /// </summary>
    /// <remarks>Using individual classes for sorting strategies to actually do the sorting seems a bit overkill. Implementing still for demonstration purpose of showing different design patterns.</remarks>
    public interface ISortingFactory
    {
        /// <summary>
        /// Determines and create the sorting strategy implementation based from provided SortType.
        /// </summary>
        /// <param name="sortType">The sort type to use.</param>
        /// <returns>The sorting strategy implementation.</returns>
        ISortingStrategy CreateSorter(SortingType sortType);
    }
}