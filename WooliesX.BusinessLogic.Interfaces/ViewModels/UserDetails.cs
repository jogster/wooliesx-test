﻿using Newtonsoft.Json;

namespace WooliesX.BusinessLogic.Interfaces.ViewModels
{
    /// <summary>
    /// Defines user related details.
    /// </summary>
    public class UserDetails
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}