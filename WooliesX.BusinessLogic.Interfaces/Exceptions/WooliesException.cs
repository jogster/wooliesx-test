﻿using System;
using WooliesX.BusinessLogic.Interfaces.Enums;

namespace WooliesX.BusinessLogic.Interfaces.Exceptions
{
    /// <summary>
    /// Generic exception specific for WooliesX application.
    /// </summary>
    public class WooliesException : Exception
    {
        /// <summary>
        /// Creates a new exception with message provided.
        /// </summary>
        /// <param name="message">The exception's message.</param>
        public WooliesException(string message) : base(message)
        {
        }

        /// <summary>
        /// Gets or sets the exception's reason.
        /// </summary>
        public FaultReason Reason { get; set; }
    }
}
