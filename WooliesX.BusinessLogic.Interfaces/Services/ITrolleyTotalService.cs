﻿using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;

namespace WooliesX.BusinessLogic.Interfaces.Services
{
    /// <summary>
    /// Provides interface definitions for the TrolleyTotal endpoint.
    /// </summary>
    public interface ITrolleyTotalService
    {
        /// <summary>
        /// Compute the total of provided trolley items using the Calculator API.
        /// </summary>
        /// <param name="trolleyTotalRequest">The trolley total request object.</param>
        /// <returns>The total of all items with specials applied.</returns>
        Task<string> ComputeTrolleyTotalWithCalculatorApi(string trolleyTotalRequestJson);
    }
}
