﻿using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;

namespace WooliesX.BusinessLogic.Interfaces.Services
{
    /// <summary>
    /// Provides interface definitions for sorting products.
    /// </summary>
    public interface IProductSortService
    {
        /// <summary>
        /// Sort the customer products collection given a sorting type to use.
        /// </summary>
        /// <param name="sortType">The sorting type to use.</param>
        /// <returns>An awaitable task.</returns>
        Task<IOrderedEnumerable<Product>> SortProducts(string sortType);
    }
}