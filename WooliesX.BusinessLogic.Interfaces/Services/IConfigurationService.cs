﻿using System.Threading.Tasks;

namespace WooliesX.BusinessLogic.Interfaces.Services
{
    /// <summary>
    /// Provides interface definition for reading configured values.
    /// </summary>
    public interface IConfigurationService
    {
        /// <summary>
        /// Get a configuration value.
        /// </summary>
        /// <param name="key">The config key.</param>
        /// <returns>The config value.</returns>
        Task<string> GetConfigurationValue(string key);
    }
}