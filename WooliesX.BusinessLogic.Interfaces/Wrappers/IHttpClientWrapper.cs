﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WooliesX.BusinessLogic.Interfaces.Wrappers
{
    /// <summary>
    /// Provides interface definitions for wrapping HttpClient method/s.
    /// </summary>
    public interface IHttpClientWrapper : IDisposable
    {
        Task<HttpResponseMessage> PostAsync(string url, HttpContent content);

        Task<HttpResponseMessage> GetAsync(string url);
    }
}