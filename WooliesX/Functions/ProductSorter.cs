using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using WooliesX.BusinessLogic.Interfaces.Services;
using System.IO;
using Newtonsoft.Json;
using WooliesX.BusinessLogic.Interfaces.Exceptions;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using System;

namespace WooliesX.Functions
{
    public class ProductSorter
    {
        /// <summary>
        /// The Product Sort Service.
        /// </summary>
        private readonly IProductSortService productSortService;

        /// <summary>
        /// Creates a new instance of <see cref="ProductSorter"/> class.
        /// </summary>
        /// <param name="producSortService">The product sorter.</param>
        public ProductSorter(IProductSortService producSortService)
        {
            this.productSortService = producSortService;
        }

        [FunctionName("ProductSorter")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get" ,"post", Route = "sort")] HttpRequest req,
            ILogger log)
        {
            try
            {
                string sortOption = req.Query["sortOption"];
                log.LogInformation("Sort customer products request received.");

                var postOption = string.Empty;
                SortProductsRequest postRequest = null;

                using (StreamReader streamReader = new StreamReader(req.Body))
                {
                    var incomingRequest = await new StreamReader(req.Body).ReadToEndAsync();
                    postRequest = JsonConvert.DeserializeObject<SortProductsRequest>(incomingRequest);
                }

                sortOption = sortOption ?? postRequest?.SortOption;

                var result = await productSortService.SortProducts(sortOption);

                if (result != null)
                {
                    return new OkObjectResult(result);
                }
                else
                {
                    log.LogWarning("Something went wrong while sorting. A null result was received. Please check data.");
                    return new BadRequestResult();
                }
            }
            catch (WooliesException we)
            {
                log.LogError("Unexpected WooliesException was thrown. Reason: {Reason}. Message: {Message}", we.Reason, we.Message);
                return new BadRequestResult();
            }
            catch(Exception e)
            {
                log.LogError("Unexpected Exception was thrown. Message: {Message}", e.Message);
                return new BadRequestResult();
            }
        }
    }
}