using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using WooliesX.BusinessLogic.Interfaces.Builders;
using WooliesX.BusinessLogic.Interfaces.Services;
using System;

namespace WooliesX.Functions
{
    /// <summary>
    /// Azure Function class for providing user details API endpoint.
    /// </summary>
    public class UserDetails
    {
        /// <summary>
        /// The <see cref="IUserBuilder"/>.
        /// </summary>
        private readonly IUserBuilder userBuilder;

        /// <summary>
        /// The <see cref="IConfigurationService"/> implementation.
        /// </summary>
        private readonly IConfigurationService configService;

        /// <summary>
        /// Instantiates a new instance of <see cref="UserDetails"/> class.
        /// </summary>
        public UserDetails(IConfigurationService configuration, IUserBuilder userBuilder)
        {
            this.configService = configuration;
            this.userBuilder = userBuilder;
        }

        /// <summary>
        /// Runs the function execution.
        /// </summary>
        /// <param name="req">The API request.</param>
        /// <param name="log">Logger.</param>
        /// <returns>An IActionResult task.</returns>
        [FunctionName("UserDetails")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "user")] HttpRequest req,
            ILogger log)
        {
            try
            {
                log.LogInformation("User resource request received.");

                var name = await configService.GetConfigurationValue("name");
                var uniqueId = await configService.GetConfigurationValue("uniqueId");

                var result = await this.userBuilder.CreateUserDetails(name, uniqueId);

                return new OkObjectResult(result);
            }
            catch(Exception e)
            {
                log.LogError(e, "An unexpected error occured while trying to get user details. {Message}", e.Message);
                return new BadRequestObjectResult(e);
            }
        }
    }
}