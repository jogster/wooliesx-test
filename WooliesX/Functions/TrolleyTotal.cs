using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using WooliesX.BusinessLogic.Interfaces.Services;
using System;

namespace WooliesX.Functions
{
    /// <summary>
    /// Function endpoint providing trolley total compute service.
    /// </summary>
    public class TrolleyTotal
    {
        /// <summary>
        /// The <see cref="ITrolleyTotalService"/>.
        /// </summary>
        private readonly ITrolleyTotalService trolleyTotalService;

        /// <summary>
        /// Creates a new instance of the TrolleyTotal function class.
        /// </summary>
        /// <param name="trolleyTotalService"></param>
        public TrolleyTotal(ITrolleyTotalService trolleyTotalService)
        {
            this.trolleyTotalService = trolleyTotalService;
        }

        [FunctionName("TrolleyTotal")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "trolleyTotal")] HttpRequest req,
            ILogger log)
        {
            try
            {
                log.LogInformation("Trolley total request was received.");

                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

                log.LogInformation("request body received for trolley total: {Body}", requestBody);

                //NOTE: commented code below is part of code should we implement trolly total locally.
                //var data = JsonConvert.DeserializeObject<TrolleyTotalRequest>(requestBody);

                var trolleyTotal = await trolleyTotalService.ComputeTrolleyTotalWithCalculatorApi(requestBody);

                if (!string.IsNullOrEmpty(trolleyTotal))
                {
                    return new OkObjectResult(trolleyTotal);
                }

                log.LogWarning("Something went wrong while retrieving product total.");
                return new BadRequestResult();
            }
            catch(Exception e)
            {
                log.LogError(e, "An unexpected error was encountered while getting trolley's total value. {Message}", e.Message);
                return new BadRequestResult();
            }
        }
    }
}
