﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Linq;
using WooliesX;
using WooliesX.BusinessLogic.Builders;
using WooliesX.BusinessLogic.Interfaces.Builders;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Sorting;
using WooliesX.BusinessLogic.Interfaces.Wrappers;
using WooliesX.BusinessLogic.Services;
using WooliesX.BusinessLogic.Sorting;
using WooliesX.BusinessLogic.Wrappers;

[assembly: FunctionsStartup(typeof(Startup))]
namespace WooliesX
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddLogging();
            var build = IFunctionsHostBuilderConfigurationExtensions.AddAppSettingsToConfiguration(builder);

            build.Services.Add(new ServiceDescriptor(typeof(IUserBuilder), typeof(UserBuilder), ServiceLifetime.Transient));
            build.Services.Add(new ServiceDescriptor(typeof(IHttpClientWrapper), typeof(HttpClientWrapper), ServiceLifetime.Singleton));
            build.Services.Add(new ServiceDescriptor(typeof(IProductSortService), typeof(ProductSortService), ServiceLifetime.Transient));
            build.Services.Add(new ServiceDescriptor(typeof(ISortingFactory), typeof(SortingFactory), ServiceLifetime.Transient));
            build.Services.Add(new ServiceDescriptor(typeof(IConfigurationService), typeof(ConfigurationService), ServiceLifetime.Transient));
            build.Services.Add(new ServiceDescriptor(typeof(ITrolleyTotalService), typeof(TrolleyTotalService), ServiceLifetime.Transient));
        }
    }

    static class IFunctionsHostBuilderConfigurationExtensions
    {
        public static IFunctionsHostBuilder AddAppSettingsToConfiguration(this IFunctionsHostBuilder builder)
        {
            var currentDirectory = "/home/site/wwwroot";
            bool isLocal = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_INSTANCE_ID"));
            if (isLocal)
            {
                currentDirectory = Environment.CurrentDirectory;
            }

            var tmpConfig = new ConfigurationBuilder()
                .SetBasePath(currentDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            var environmentName = tmpConfig["Environment"];

            var configurationBuilder = new ConfigurationBuilder();

            var descriptor = builder.Services.FirstOrDefault(d => d.ServiceType == typeof(IConfiguration));
            if (descriptor?.ImplementationInstance is IConfiguration configRoot)
            {
                configurationBuilder.AddConfiguration(configRoot);
            }

            var configuration = configurationBuilder.SetBasePath(currentDirectory)
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true)
                .Build();

            builder.Services.Replace(ServiceDescriptor.Singleton(typeof(IConfiguration), configuration));

            return builder;
        }
    }
}
