﻿using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Builders;
using WooliesX.BusinessLogic.Interfaces.ViewModels;

namespace WooliesX.BusinessLogic.Builders
{
    /// <summary>
    /// Implements methods defined on <see cref="IUserBuilder"/> interface.
    /// </summary>
    public class UserBuilder : IUserBuilder
    {
        /// <summary>
        /// Create a <see cref="UserDetails"/> object.
        /// </summary>
        /// <param name="name">The name field.</param>
        /// <param name="token">The token/unique id field.</param>
        /// <returns>A new user details object, pre-filled with configured values.</returns>
        public Task<UserDetails> CreateUserDetails(string name, string token)
        {
            var result = new UserDetails()
            {
                Name = name,
                Token = token,
            };

            return Task.FromResult(result);
        }
    }
}
