﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Interfaces.Enums;
using WooliesX.BusinessLogic.Interfaces.Exceptions;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Sorting;
using WooliesX.BusinessLogic.Interfaces.Wrappers;

namespace WooliesX.BusinessLogic.Services
{
    public class ProductSortService : IProductSortService
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger<ProductSortService> logger;

        /// <summary>
        /// An instance of <see cref="IHttpClientWrapper"/> object.
        /// </summary>
        private readonly IHttpClientWrapper httpClient;

        /// <summary>
        /// The <see cref="ISortingFactory"/>.
        /// </summary>
        private readonly ISortingFactory sortingFactory;

        /// <summary>
        /// The <see cref="IConfigurationService"/>.
        /// </summary>
        private readonly IConfigurationService configService;

        /// <summary>
        /// Creates a new instance of <see cref="ProductSortService"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="httpClientWrapper">The HttpClient wrapper.</param>
        /// <param name="sortingFactory">The sorting strategy factory.</param>
        /// <param name="configService">The configuration service.</param>
        public ProductSortService(ILogger<ProductSortService> logger, IHttpClientWrapper httpClientWrapper, ISortingFactory sortingFactory, IConfigurationService configService)
        {
            this.logger = logger;
            this.httpClient = httpClientWrapper;
            this.sortingFactory = sortingFactory;
            this.configService = configService;
        }

        /// <summary>
        /// Sort the customer products collection given a sorting type to use.
        /// </summary>
        /// <param name="sortType">The sorting type to use.</param>
        /// <returns>An awaitable task.</returns>
        public async Task<IOrderedEnumerable<Product>> SortProducts(string sortType)
        {
            try
            {
                //try parsing sortType first to confirm if it's a valid one. 
                if(!Enum.TryParse(sortType, true, out SortingType sortingType))
                {
                    this.logger.LogError($"Unknown sort type {sortType} was passed. Terminating request.");
                    throw new WooliesException($"Unknown sort type string {sortType} was passed. Unable to proceed.")
                    {
                        Reason = FaultReason.UnknownSortingType
                    };
                }

                var token = await this.configService.GetConfigurationValue("uniqueId");
                var baseUrl = await this.configService.GetConfigurationValue("productsResourceBaseUrl");

                var url = baseUrl + token;
                var response = await httpClient.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var customerProducts = JsonConvert.DeserializeObject<IList<Product>>(content);

                    this.logger.LogInformation("Successfully retrieved list of customer products. {Products}", customerProducts);

                    var sorter = this.sortingFactory.CreateSorter(sortingType);
                    return await sorter.SortCustomerProducts(customerProducts);
                }

                return null;
            }
            catch (WooliesException)
            {
                throw;
            }
            catch(Exception e)
            {
                this.logger.LogError(e, "Unexpected exception encountered while trying to sort products. Message: {Message}", e.Message);
                throw;
            }
        }
    }
}
