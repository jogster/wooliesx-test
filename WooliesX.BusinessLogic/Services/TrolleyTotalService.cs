﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Wrappers;

namespace WooliesX.BusinessLogic.Services
{
    /// <summary>
    /// Provides trolley total computation service.
    /// </summary>
    public class TrolleyTotalService : ITrolleyTotalService
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger<TrolleyTotalService> logger;

        /// <summary>
        /// An instance of <see cref="IHttpClientWrapper"/> object.
        /// </summary>
        private readonly IHttpClientWrapper httpClient;

        /// <summary>
        /// The <see cref="IConfigurationService"/>.
        /// </summary>
        private readonly IConfigurationService configService;

        /// <summary>
        /// Creates a new instance of the <see cref="TrolleyTotalService"/> class.
        /// </summary>
        /// <param name="configService">The config service.</param>
        /// <param name="httpClientWrapper">The Http Client wrapper.</param>
        /// <param name="logger">The logger.</param>
        public TrolleyTotalService(ILogger<TrolleyTotalService> logger, IHttpClientWrapper httpClientWrapper, IConfigurationService configService)
        {
            this.logger = logger;
            this.httpClient = httpClientWrapper;
            this.configService = configService;
        }

        /// <summary>
        /// Compute the total of provided trolley items using the Calculator API.
        /// </summary>
        /// <param name="trolleyTotalRequest">The trolley total request object.</param>
        /// <returns>The total of all items with specials applied.</returns>
        public async Task<string> ComputeTrolleyTotalWithCalculatorApi(string trolleyTotalRequestJson)
        {
            this.logger.LogInformation("Compute Trolley Total request from TrolleyTotalService.");

            var token = await this.configService.GetConfigurationValue("uniqueId");
            var baseUrl = await this.configService.GetConfigurationValue("trolleyTotalResourceBaseUrl");

            var url = baseUrl + token;

            var data = new StringContent(trolleyTotalRequestJson, System.Text.Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, data);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var trolleyTotal = JsonConvert.DeserializeObject<string>(content);

                this.logger.LogInformation("Successfully computed trolley total using calculator. {Content}", content);

                return trolleyTotal;
            }

            return string.Empty;
        }
    }
}
