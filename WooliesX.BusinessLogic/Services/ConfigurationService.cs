﻿using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Services;

namespace WooliesX.BusinessLogic.Services
{
    /// <summary>
    /// Provides concrete implementation of <see cref="IConfigurationService"/>.
    /// </summary>
    public class ConfigurationService : IConfigurationService
    {
        /// <summary>
        /// The configuration provider.
        /// </summary>
        private readonly IConfiguration configuration;

        /// <summary>
        /// Creates a new instance of the configuration service.
        /// </summary>
        public ConfigurationService(IConfiguration config)
        {
            this.configuration = config;
        }

        public Task<string> GetConfigurationValue(string key)
        {
            var result = configuration[key];
            return Task.FromResult(result);
        }
    }
}