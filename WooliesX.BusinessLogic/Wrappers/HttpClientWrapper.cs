﻿using System.Net.Http;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Wrappers;

namespace WooliesX.BusinessLogic.Wrappers
{
    /// <summary>
    /// Wraps the <see cref="HttpClient"/> object.
    /// </summary>
    public class HttpClientWrapper : IHttpClientWrapper
    {
        /// <summary>
        /// The actual HttpClient being wrapped.
        /// </summary>
        private readonly HttpClient _client;

        public HttpClientWrapper()
        {
            _client = new HttpClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public Task<HttpResponseMessage> GetAsync(string url)
        {
            return _client.GetAsync(url);
        }

        public Task<HttpResponseMessage> PostAsync(string url, HttpContent content)
        {
            return _client.PostAsync(url, content);
        }
    }
}
