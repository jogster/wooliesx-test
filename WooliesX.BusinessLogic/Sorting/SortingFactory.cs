﻿using WooliesX.BusinessLogic.Interfaces.Enums;
using WooliesX.BusinessLogic.Interfaces.Exceptions;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Sorting;
using WooliesX.BusinessLogic.Interfaces.Wrappers;

namespace WooliesX.BusinessLogic.Sorting
{
    /// <summary>
    /// Creates appropriate sorter implementation 
    /// </summary>
    public class SortingFactory : ISortingFactory
    {
        /// <summary>
        /// The <see cref="IHttpClientWrapper"/>.
        /// </summary>
        private readonly IHttpClientWrapper httpClient;

        /// <summary>
        /// The <see cref="IConfigurationService"/>.
        /// </summary>
        private readonly IConfigurationService configService;

        /// <summary>
        /// Creates a new instance of the <see cref="SortingFactory"/> class.
        /// </summary>
        /// <param name="httpClientWrapper"></param>
        /// <param name="configService"></param>
        public SortingFactory(IHttpClientWrapper httpClientWrapper, IConfigurationService configService)
        {
            this.httpClient = httpClientWrapper;
            this.configService = configService;
        }

        /// <summary>
        /// Determines and create the sorting strategy implementation based from provided SortType.
        /// </summary>
        /// <param name="sortType">The sort type to use.</param>
        /// <returns>The sorting strategy implementation.</returns>
        /// <remarks>Using individual classes for sorting strategies to actually do the sorting seems a bit overkill. 
        /// However, still implementing like so to demonstrate Factory & Strategy Pattern.</remarks>
        public ISortingStrategy CreateSorter(SortingType sortType)
        {
            switch (sortType)
            {
                case SortingType.Low:
                    {
                        return new LowToHighPriceSorting();
                    }
                case SortingType.High:
                    {
                        return new HighToLowSorting();
                    }
                case SortingType.Ascending:
                    {
                        return new AscendingNameSorting();
                    }
                case SortingType.Descending:
                    {
                        return new DescendingNameSorting();
                    }
                case SortingType.Recommended:
                    {
                        return new RecommendedProductsSorting(this.httpClient, this.configService);
                    }
                default:
                    {
                        throw new WooliesException("Unexpected sorting type was encountered.")
                        {
                            Reason = FaultReason.UnknownSortingType
                        };
                    }
            }
        }
    }
}