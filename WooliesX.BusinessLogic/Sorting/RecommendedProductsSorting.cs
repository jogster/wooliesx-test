﻿using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.Sorting;
using WooliesX.BusinessLogic.Interfaces.Wrappers;

namespace WooliesX.BusinessLogic.Sorting
{
    /// <summary>
    /// Sort products by customer popularity.
    /// </summary>
    public class RecommendedProductsSorting : ISortingStrategy
    {
        /// <summary>
        /// The <see cref="IHttpClientWrapper"/>.
        /// </summary>
        private readonly IHttpClientWrapper httpClient;

        /// <summary>
        /// The <see cref="IConfigurationService"/>.
        /// </summary>
        private readonly IConfigurationService configService;

        /// <summary>
        /// Creates a new instance of the <see cref="RecommendedProductsSorting"/> class.
        /// </summary>
        /// <param name="httpClientWrapper"></param>
        /// <param name="configService"></param>
        public RecommendedProductsSorting(IHttpClientWrapper httpClientWrapper, IConfigurationService configService)
        {
            this.httpClient = httpClientWrapper;
            this.configService = configService;
        }

        /// <summary>
        /// Sorts the given collection of customer products.
        /// </summary>
        /// <param name="productsList">Products to sort.</param>
        /// <returns>A collection of sorted products.</returns>
        public async Task<IOrderedEnumerable<Product>> SortCustomerProducts(IList<Product> productsList)
        {
            var token = await this.configService.GetConfigurationValue("uniqueId");
            var baseUrl = await this.configService.GetConfigurationValue("shopperHistoryResourceBaseUrl");

            var url = baseUrl + token;
            var response = await httpClient.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var customers = JsonConvert.DeserializeObject<IList<CustomerProducts>>(content);

                return ProjectAndSortHistoricalProductOrders(customers, productsList);
            }

            return null;
        }

        /// <summary>
        /// Iterate through the historical product purchase per customer recording data to a dictionary, project to expected list of <see cref="Product"/> format
        /// and do final sort with popular items first.
        /// </summary>
        /// <param name="customers">List of customer transactions.</param>
        /// <param name="productList">List of products.</param>
        /// <returns></returns>
        private IOrderedEnumerable<Product> ProjectAndSortHistoricalProductOrders(IList<CustomerProducts> customers, IList<Product> productList)
        {
            var popularityCount = new ConcurrentDictionary<string, long>();
            var priceRecord = new Dictionary<string, double>();

            foreach (var customer in customers)
            {
                foreach (var product in customer.ShoppingProducts)
                {
                    popularityCount.AddOrUpdate(product.Name, product.Quantity, (k, v) => v + product.Quantity);
                    priceRecord.TryAdd(product.Name, product.Price);
                }
            }

            var popularProducts = new List<Product>();

            foreach (var product in popularityCount)
            {
                popularProducts.Add(new Product()
                {
                    Name = product.Key,
                    Price = priceRecord[product.Key],
                    Quantity = product.Value
                });
            }

            foreach(var product in productList)
            {
                if (!popularProducts.Any(x => string.Compare(x.Name, product.Name, System.StringComparison.InvariantCultureIgnoreCase) == 0))
                {
                    popularProducts.Add(new Product()
                    {
                        Name = product.Name,
                        Price = product.Price,
                        Quantity = 0
                    });
                }
            }

            return popularProducts.OrderByDescending(x => x.Quantity);
        }
    }
}