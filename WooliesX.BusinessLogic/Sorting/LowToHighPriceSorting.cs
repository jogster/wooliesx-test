﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.DataObjects;
using WooliesX.BusinessLogic.Interfaces.Sorting;

namespace WooliesX.BusinessLogic.Sorting
{
    /// <summary>
    /// Sorts by price in ascending order.
    /// </summary>
    public class LowToHighPriceSorting : ISortingStrategy
    {
        /// <summary>
        /// Sorts the given collection of customer products.
        /// </summary>
        /// <param name="productsList">Products to sort.</param>
        /// <returns>A collection of sorted products.</returns>
        /// <remarks>A bit overkill to have these 1 liner sorting implementation on a new class but I wanted to demonstrate 
        /// StrategyPattern hence this.</remarks>
        public Task<IOrderedEnumerable<Product>> SortCustomerProducts(IList<Product> productsList)
        {
            return Task.FromResult(productsList.OrderBy(x => x.Price));
        }
    }
}