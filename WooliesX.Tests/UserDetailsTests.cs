﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Builders;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.ViewModels;

namespace WooliesX.Tests
{
    public class UserDetailsTests : BaseWooliesXTest
    {
        private Functions.UserDetails _sut;
        private Mock<IConfigurationService> configService;
        private Mock<IUserBuilder> userBuilder;
        private Mock<ILogger> logger;

        [SetUp]
        public void Setup()
        {
            configService = new Mock<IConfigurationService>();
            userBuilder = new Mock<IUserBuilder>();
            logger = new Mock<ILogger>();
            _sut = new Functions.UserDetails(configService.Object, userBuilder.Object);
        }

        [Test]
        public async Task ValidScenario_Run_AppropriateStepsRan()
        {
            var userDetails = new UserDetails()
            {
                Token = "123",
                Name = "Test Person",
            };

            var request = CreateMockRequest("{\"test\":\"123\"}");

            this.configService.Setup(x => x.GetConfigurationValue("uniqueId")).ReturnsAsync("123");
            this.configService.Setup(x => x.GetConfigurationValue("name")).ReturnsAsync("Test Person");
            this.userBuilder.Setup(x => x.CreateUserDetails("Test Person", "123")).ReturnsAsync(userDetails);

            var result = await _sut.Run(request.Object, this.logger.Object);
            var mvcResult = (OkObjectResult)result;
            var output = (UserDetails)mvcResult.Value;

            result.ShouldNotBeNull();
            output.Name.ShouldBe("Test Person");
            output.Token.ShouldBe("123");
            this.configService.Verify(x => x.GetConfigurationValue("uniqueId"), Times.Once);
            this.configService.Verify(x => x.GetConfigurationValue("name"), Times.Once);
            this.userBuilder.Verify(x => x.CreateUserDetails("Test Person", "123"), Times.Once);
        }

        [Test]
        public async Task UserBuilderThrowsException_Run_BadRequestReturned()
        {
            var userDetails = new UserDetails()
            {
                Token = "123",
                Name = "Test Person",
            };

            var exception = new Exception("test error message");

            var request = CreateMockRequest("{\"test\":\"123\"}");

            this.configService.Setup(x => x.GetConfigurationValue("uniqueId")).ReturnsAsync("123");
            this.configService.Setup(x => x.GetConfigurationValue("name")).ReturnsAsync("Test Person");
            this.userBuilder.Setup(x => x.CreateUserDetails("Test Person", "123")).ThrowsAsync(exception);

            var result = await _sut.Run(request.Object, this.logger.Object);
            var mvcResult = (BadRequestObjectResult)result;

            result.ShouldNotBeNull();
            mvcResult.Value.ShouldBe(exception);
            this.configService.Verify(x => x.GetConfigurationValue("uniqueId"), Times.Once);
            this.configService.Verify(x => x.GetConfigurationValue("name"), Times.Once);
        }
    }
}
