using Microsoft.AspNetCore.Http;
using Moq;
using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace WooliesX.Tests
{
    public class BaseWooliesXTest
    {
        internal Mock<HttpRequest> CreateMockRequest(object body)
        {
            var json = JsonConvert.SerializeObject(body);
            var byteArray = Encoding.ASCII.GetBytes(json);

            var _memoryStream = new MemoryStream(byteArray);
            _memoryStream.Flush();
            _memoryStream.Position = 0;

            var mockRequest = new Mock<HttpRequest>();
            mockRequest.Setup(x => x.Body).Returns(_memoryStream);

            return mockRequest;
        }
    }
}