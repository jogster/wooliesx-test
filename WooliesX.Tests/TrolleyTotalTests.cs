﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Interfaces.Builders;
using WooliesX.BusinessLogic.Interfaces.Services;
using WooliesX.BusinessLogic.Interfaces.ViewModels;

namespace WooliesX.Tests
{
    public class TrolleyTotalTests : BaseWooliesXTest
    {
        private Functions.TrolleyTotal _sut;
        private Mock<ITrolleyTotalService> trolleyTotalService;
        private Mock<ILogger> logger;

        [SetUp]
        public void Setup()
        {
            trolleyTotalService = new Mock<ITrolleyTotalService>();
            logger = new Mock<ILogger>();
            _sut = new Functions.TrolleyTotal(trolleyTotalService.Object);
        }

        [Test]
        public async Task ValidScenario_Run_AppropriateStepsRan()
        {
            var testDataJson = "{\"Products\":[{\"Name\":\"1\",\"Price\":2.0},{\"Name\":\"2\",\"Price\":5.0}],\"Specials\":[{\"Quantities\":[{\"Name\":\"1\",\"Quantity\":3},{\"Name\":\"2\",\"Quantity\":0}],\"Total\":5.0},{\"Quantities\":[{\"Name\":\"1\",\"Quantity\":1},{\"Name\":\"2\",\"Quantity\":2}],\"Total\":10.0}],\"Quantities\":[{\"Name\":\"1\",\"Quantity\":3},{\"Name\":\"2\",\"Quantity\":2}]}";

            var request = CreateMockRequest(testDataJson);

            this.trolleyTotalService.Setup(x => x.ComputeTrolleyTotalWithCalculatorApi(It.IsAny<string>())).ReturnsAsync("100");

            var result = await _sut.Run(request.Object, this.logger.Object);
            var mvcResult = (OkObjectResult)result;

            result.ShouldNotBeNull();
            mvcResult.Value.ShouldBe("100");
        }

        [Test]
        public async Task EmptyTotalStringReturned_Run_ErrorResponse()
        {
            var testDataJson = "{\"Products\":[{\"Name\":\"1\",\"Price\":2.0},{\"Name\":\"2\",\"Price\":5.0}],\"Specials\":[{\"Quantities\":[{\"Name\":\"1\",\"Quantity\":3},{\"Name\":\"2\",\"Quantity\":0}],\"Total\":5.0},{\"Quantities\":[{\"Name\":\"1\",\"Quantity\":1},{\"Name\":\"2\",\"Quantity\":2}],\"Total\":10.0}],\"Quantities\":[{\"Name\":\"1\",\"Quantity\":3},{\"Name\":\"2\",\"Quantity\":2}]}";

            var request = CreateMockRequest(testDataJson);

            this.trolleyTotalService.Setup(x => x.ComputeTrolleyTotalWithCalculatorApi(It.IsAny<string>())).ReturnsAsync(string.Empty);

            var result = await _sut.Run(request.Object, this.logger.Object);
            var mvcResult = (BadRequestResult)result;

            mvcResult.ShouldNotBeNull();
            
        }
    }
}
